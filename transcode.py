#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: Youngson
# @Eamil:  Youngson.Gu@gmail.com
# @Date:   2016-07-30 9:22
# @Last Modified by:   Youngson
# @Last Modified time: 2016-07-30 9:22

from os import path, getcwd, listdir, mkdir

path_b = path.join(getcwd(), 'resources')
path_r = path.join(path_b, 'messages_zh_CN')
path_w = path.join(path_b, 'messages')
if not path.exists(path_w):
    mkdir(path_w)
for files in listdir(path_r):
    with open(path.join(path_r, files), encoding='utf-8') as rf:
        with open(path.join(path_w, files), 'w', encoding='utf-8') as wf:
            for line in rf:
                wf.writelines(
                    str(line.strip().encode('unicode-escape')).replace('\\\\u', '\\u')[2:-1]+'\n')
